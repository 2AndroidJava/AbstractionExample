package abstraction.amen.com.abstractionexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.LinkedList;

import abstraction.amen.com.abstractionexample.interfaces.IDescriptable;
import abstraction.amen.com.abstractionexample.interfaces.IListable;
import abstraction.amen.com.abstractionexample.interfaces.IManageable;
import abstraction.amen.com.abstractionexample.models.ProductFour;
import abstraction.amen.com.abstractionexample.models.ProductOne;
//import abstraction.amen.com.abstractionexample.models.ProductTwo;
import abstraction.amen.com.abstractionexample.models.ProductThree;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.listView)
    protected ListView listView;

    @BindView(R.id.textView)
    protected TextView textView;

    private ArrayAdapter<IListable> listAdapter;

    @OnItemClick(R.id.listView)
    protected void itemClicked(AdapterView<?> parent, View view, int position, long id){
        IListable listableItem = listAdapter.getItem(position);

        // sprawdzamy czy obiekt jest descriptable i manageable
        if((listableItem instanceof IDescriptable) &&
                (listableItem instanceof IManageable)){
            IManageable castInst = (IManageable) listableItem;

            // start nowego activity z manageable id
            Intent i = new Intent(this, DetailsActivity.class);
            i.putExtra("manageable_id", castInst.getId());
            startActivity(i);
        }else{
            // jeśli nie jest wyświetlamy komunikat
            Toast.makeText(this, "This object is not descriptable!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // tworzymy pusty adapter
        listAdapter = new ArrayAdapter<IListable>(
                getBaseContext(),
                android.R.layout.simple_list_item_1,
                new LinkedList<IListable>());
        // łączymy listę(element interfejsu) z adapterem
        listView.setAdapter(listAdapter);

        // wypełnienie listy
        //...
        fillList();
    }


    // dopisywanie elementów (żeby nie tworzyć interfejsów do dodawania elementów)
    private void fillList() {

        ProductOne p1 = new ProductOne("Descrpition 1 ", "name 1", 1);
        listAdapter.add(p1);
        ProductManager.getInstance().addProduct(p1);

        ProductOne p2 = new ProductOne("Descrpition 2 ", "name 2", 2);
        listAdapter.add(p2);
        ProductManager.getInstance().addProduct(p2);

        ProductOne p3 = new ProductOne("Descrpition 3 ", "name 3", 3);
        listAdapter.add(p3);
        ProductManager.getInstance().addProduct(p3);

        ProductOne p4 = new ProductOne("Descrpition 4 ", "name 4", 4);
        listAdapter.add(p4);
        ProductManager.getInstance().addProduct(p4);

        ProductOne p5 = new ProductOne("Descrpition 5 ", "name 5", 5);
        listAdapter.add(p5);
        ProductManager.getInstance().addProduct(p5);

        ProductThree p6 = new ProductThree("Descrpition 6 ", "name 6", 6);
        listAdapter.add(p6);
        ProductManager.getInstance().addProduct(p6);


        ProductFour p7 = new ProductFour("Descrpition 7 ", 7);
        listAdapter.add(p7);
        ProductManager.getInstance().addProduct(p7);

        ProductFour p8 = new ProductFour("Descrpition 8 ", 8);
        listAdapter.add(p8);
        ProductManager.getInstance().addProduct(p8);

//        listAdapter.add(new ProductTwo("Descrpition 5 "));
    }
}
