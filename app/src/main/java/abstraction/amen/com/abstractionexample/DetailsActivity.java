package abstraction.amen.com.abstractionexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import abstraction.amen.com.abstractionexample.interfaces.IDescriptable;
import abstraction.amen.com.abstractionexample.interfaces.IManageable;
import abstraction.amen.com.abstractionexample.interfaces.IUpdatable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailsActivity extends AppCompatActivity {

    @BindView(R.id.textViewDetails)
    protected TextView textViewDetails;

    @BindView(R.id.button)
    protected Button editButton;

    private IDescriptable descriptable;

    @OnClick(R.id.button)
    protected void onClickUpdate(){
        if(descriptable instanceof IUpdatable){
            IUpdatable updateable = (IUpdatable) descriptable;
            updateable.update();
        }
    }
    //519 088 399
    // pawel.reclaw@gmail.com

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        int received_id = getIntent().getIntExtra("manageable_id", -1);
        if(received_id != -1) {
            IManageable object = ProductManager.getInstance().getProduct(received_id);
            fillViews(object);
        }
    }

    private void fillViews(IManageable object) {
        if(object instanceof IDescriptable){
            descriptable = (IDescriptable) object;
            textViewDetails.setText(descriptable.getContent());
        }
        editButton.setEnabled(object instanceof IUpdatable);
    }
}
