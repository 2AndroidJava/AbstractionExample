package abstraction.amen.com.abstractionexample.interfaces;

/**
 * Created by amen on 3/20/17.
 */

public interface IDescriptable {
    String getName();
    void setName(String pName);

    String getContent();
    void setContent(String pContent);
}
