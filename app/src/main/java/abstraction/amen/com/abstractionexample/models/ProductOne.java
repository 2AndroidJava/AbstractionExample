package abstraction.amen.com.abstractionexample.models;

import abstraction.amen.com.abstractionexample.interfaces.IDescriptable;
import abstraction.amen.com.abstractionexample.interfaces.IListable;
import abstraction.amen.com.abstractionexample.interfaces.IManageable;
import abstraction.amen.com.abstractionexample.interfaces.IUpdatable;

/**
 * Created by amen on 3/20/17.
 */

public class ProductOne extends IListable implements IDescriptable {

    private int id;
    private String name;
    private String description;

    public ProductOne(String description,String name, int id) {
        this.description = description;
        this.name = name;
        this.id = id;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String toListableString() {
        return id + " " + description;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String pName) {
        this.name = pName;
    }

    @Override
    public String getContent() {
        return description;
    }

    @Override
    public void setContent(String pContent) {
        this.description = pContent;
    }

}
