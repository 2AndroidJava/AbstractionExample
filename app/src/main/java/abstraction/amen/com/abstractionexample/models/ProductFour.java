package abstraction.amen.com.abstractionexample.models;

import abstraction.amen.com.abstractionexample.interfaces.IDescriptable;
import abstraction.amen.com.abstractionexample.interfaces.IListable;
import abstraction.amen.com.abstractionexample.interfaces.IManageable;
import abstraction.amen.com.abstractionexample.interfaces.IUpdatable;

/**
 * Created by amen on 3/20/17.
 */

public class ProductFour extends IListable implements IUpdatable, IDescriptable {

    private int id;
    private String content;

    public ProductFour(String myName, int pid) {
        this.id = pid;
        this.content = myName;
    }

    @Override
    protected String toListableString() {
        return content;
    }

    @Override
    public void update() {
        content += " " + System.currentTimeMillis();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void setName(String pName) {

    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public void setContent(String pContent) {
        content = pContent;
    }
}
