package abstraction.amen.com.abstractionexample.interfaces;

/**
 * Created by amen on 3/20/17.
 */

public interface IUpdatable {

    void update();

}
