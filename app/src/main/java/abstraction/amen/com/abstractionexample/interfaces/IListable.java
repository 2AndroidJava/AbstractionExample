package abstraction.amen.com.abstractionexample.interfaces;

/**
 * Created by amen on 3/20/17.
 */

public abstract class IListable implements IManageable {

    protected abstract String toListableString();

    @Override
    public String toString(){
        return toListableString();
    }
}
