package abstraction.amen.com.abstractionexample.models;

import abstraction.amen.com.abstractionexample.interfaces.IDescriptable;
import abstraction.amen.com.abstractionexample.interfaces.IListable;
import abstraction.amen.com.abstractionexample.interfaces.IManageable;

/**
 * Created by amen on 3/20/17.
 */

public class ProductThree extends IListable {

    private int id;
    private String name;
    private String description;

    public ProductThree(String description,String name, int id) {
        this.description = description;
        this.name = name;
        this.id = id;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String toListableString() {
        return id + " " + description;
    }
}