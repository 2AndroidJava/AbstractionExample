package abstraction.amen.com.abstractionexample;

import java.util.HashMap;

import abstraction.amen.com.abstractionexample.interfaces.IManageable;

/**
 * Created by amen on 3/20/17.
 */
public class ProductManager {
    private static ProductManager ourInstance = new ProductManager();

    public static ProductManager getInstance() {
        return ourInstance;
    }

    private HashMap<Integer, IManageable> productMap = new HashMap<>();

    private ProductManager() {
    }

    public void addProduct(IManageable newProduct){
        productMap.put(newProduct.getId(), newProduct);
    }

    public IManageable getProduct(int id){
        return productMap.get(id);
    }
}
